﻿'use strict';

/**
 * @ngdoc function
 * @name liveSafeDemoApp.service:WeatherModel
 * @description
 * # WeatherModel
 */
angular.module('liveSafeDemoApp')
  .service('WeatherModel', function ($http) {
	
		this.search = function(query){
			return $http.post("http://api.openweathermap.org/data/2.5/forecast/daily?appid=585e670f55ee9b114fa2f1f2731177d9&q="+query+"&units=imperial&cnt=5");
		};
		
		this.searchLatLon = function(lat,lon){
			return $http.post("http://api.openweathermap.org/data/2.5/forecast/daily?lat="+lat+"&lon="+lon+"&appid=585e670f55ee9b114fa2f1f2731177d9&units=imperial&cnt=5");
		};
	});
