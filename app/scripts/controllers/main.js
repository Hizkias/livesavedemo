'use strict';

/**
 * @ngdoc function
 * @name liveSafeDemoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the liveSafeDemoApp
 */
angular.module('liveSafeDemoApp')
  .controller('MainCtrl', function ($scope, WeatherModel) {
		
		window.navigator.geolocation.getCurrentPosition(function(pos){
			$scope.loadingResults = true;
			WeatherModel.searchLatLon(pos.coords.latitude,pos.coords.longitude).then(function(resp){
					$scope.forecast = resp.data.list;
					console.log(resp);
					$scope.loadedQuery = resp.data.city.name;
					$scope.loaded = true;
					$scope.loadingResults = false;
			});
		});
		
		$scope.search = function(){
			$scope.loadingResults = true;
			$scope.loaded = false;
			$scope.searchError = "";
			
			WeatherModel.search($scope.query).then(function(resp){
				$scope.loadingResults = false;
				if(resp.data.cod == 404){
					$scope.forecast = [];
					$scope.searchError = "Sorry, your search " + $scope.query + " did not return any results. Please check the city you entered and try again.";
				}
				
				else{
					$scope.forecast = resp.data.list;
					$scope.loaded = true;
					//$scope.loadedQuery = resp.data.city.name;
					$scope.loadedQuery = $scope.query;
					$scope.query = "";
					console.log(resp);
				}
				
			}, function(error){
				$scope.loadingResults = false;
				$scope.forecast = [];
				$scope.searchError = "We couldn't process your search at this time. Please try again.";
			});
		};
		
		$scope.visible ="day";
	});
