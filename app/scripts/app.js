'use strict';

/**
 * @ngdoc overview
 * @name liveSafeDemoApp
 * @description
 * # liveSafeDemoApp
 *
 * Main module of the application.
 */
angular
  .module('liveSafeDemoApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
		'angularSpinner'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
